﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FacadePattern
{
    public class ShopFacade
    {
        private InventoryService InventoryService { get; set; }
        private PaymentService PaymentService { get; set; }
        private RouteService RouteService { get; set; }
        private ShippingService ShippingService { get; set; }

        public ShopFacade()
        {
            InventoryService = new InventoryService();
            PaymentService = new PaymentService();
            RouteService = new RouteService();
            ShippingService = new ShippingService();
        }

        public void PlaceOrder(Order order)
        {
            bool isAvailable = InventoryService.IsAvailable(order);
            if (isAvailable == false)
            {
                throw new Exception("Order product is not available");
            }

            bool isPaid = PaymentService.IsPaid(order);
            if(isPaid == false)
            {
                throw new Exception("Order is not paid");
            }

            string route = RouteService.GenerateRoute(order);
            ShippingService.SendOrder(order, route);

            Console.WriteLine("Your order has been shipped!");
        }
    }
}
