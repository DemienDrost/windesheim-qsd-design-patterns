﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FacadePattern
{
    public class ShippingService
    {
        public void SendOrder(Order order, string route)
        {
            Console.WriteLine($"Shipping order with product id {order.ProductId}, route: {route}");
        }
    }
}
