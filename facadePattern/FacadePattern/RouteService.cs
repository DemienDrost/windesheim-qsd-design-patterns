﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FacadePattern
{
    public class RouteService
    {
        public string GenerateRoute(Order order)
        {
            return "turn left, turn right, turn right";
        }
    }
}
