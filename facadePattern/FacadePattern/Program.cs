﻿using System;

namespace FacadePattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Order order = new Order {
                ProductId = 3
            };

            ShopFacade shop = new ShopFacade();
            shop.PlaceOrder(order);

            Console.ReadLine();
        }
    }
}
