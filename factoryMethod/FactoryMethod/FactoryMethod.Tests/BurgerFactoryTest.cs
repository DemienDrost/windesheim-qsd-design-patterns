using NUnit.Framework;
using System;

namespace FactoryMethod.Tests
{

    public class BurgerFactoryTest
    {

        BurgerFactory burgerFactory = new BurgerFactory();

        [Test]
        public void ShouldCreateVeggieBurger()
        {
            var burger = burgerFactory.CreateBurger("Veggie");
            Assert.IsInstanceOf<VeggieBurger>(burger);
        }

        [Test]
        public void ShouldCreateBeefBurger()
        {
            var burger = burgerFactory.CreateBurger("Beef");
            Assert.IsInstanceOf<BeefBurger>(burger);
        }

        [Test]
        public void ShouldCreateChickenBurger()
        {
            var burger = burgerFactory.CreateBurger("Chicken");
            Assert.IsInstanceOf<ChickenBurger>(burger);
        }

        [Test]
        public void ShouldThrowNotSupportedBurgerTypeException()
        {
            var exception = Assert.Throws<ArgumentException>(() => burgerFactory.CreateBurger("BLLAH BLAH BLAAHHH"));
            Assert.AreEqual(exception.Message, "Burger type 'BLLAH BLAH BLAAHHH' is not supported");
        }
    }
}