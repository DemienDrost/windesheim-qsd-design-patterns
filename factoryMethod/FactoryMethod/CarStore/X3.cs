﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarStore
{
    public class X3 : Car
    {
        public X3(IList<string> options) : base(options)
        {
            // Het is ook mogelijk de constructor te overschrijven:
            //
            // Options.Add("Panorama roof");
        }

        public void AddPanoramaRoof()
        {
            Console.Write("\n\n X3.AddPanoramaRoof(): Adding panorama roof as option by default...\n\n");
            Options.Add("Panorama roof");
        }

        public override void AddInterior()
        {
            Console.WriteLine("Adding some beautiful interior with brown leather");
        }

        public override void Spray()
        {
            Console.WriteLine("There goes some paint");
        }

        public override void Tint() 
        {
            Console.WriteLine("Tinting the windows");
        }
    }
}
