﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarStore
{
    public class Bmw : CarStore
    {
        public override ICar CreateCar(IList<string> options)
        {
            return new X3(options);
        }

        public override ICar OrderCar(IList<string> options)
        {
            X3 car = (X3)base.OrderCar(options);

            car.AddPanoramaRoof();

            return car;
        }
    }
}