﻿using System;
using System.Collections.Generic;

namespace CarStore
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to CarStore!");
            Console.WriteLine("From which factory do you like to order your car?");

            string[] factories = { "AlfaRomeo", "Bmw" };

            foreach (string factory in factories)
            {
                Console.WriteLine($"\t - {factory}");
            }

            while (true)
            {
                Console.Write("\nCar factory choice: ");

                string selectedFactory = Console.ReadLine();

                Console.Write("\nCar options: ");

                List<string> options = new List<string>(
                    Console.ReadLine().Split(",")
                );

                CarStore factory = null;

                if (selectedFactory == "AlfaRomeo")
                {
                    factory = new AlfaRomeo();

                } else if(selectedFactory == "Bmw")
                {
                    factory = new Bmw();
                } else
                {
                    // Boem...
                }

                ICar car = factory.OrderCar(options);

                Console.WriteLine($"\nYou order: {car}");
            }
        }
    }
}
