﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarStore
{
    public class Brera : Car
    {
        public Brera(IList<string> options) : base(options)
        {
            // Het is ook mogelijk de constructor te overschrijven:
            //
            // Options.Add("Panorama roof");
        }

        public override void AddInterior()
        {
            Console.WriteLine("Adding some interior with black leather");
        }

        public override void Spray()
        {
            Console.WriteLine("There goes some paint, let's add a little rust too");
        }

        public override void Tint()
        {
            Console.WriteLine("Tinting the windows slightly");
        }
    }
}
