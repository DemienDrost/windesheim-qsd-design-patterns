﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarStore
{
    public interface ICar
    {
        public IList<string> Options { get; set; }
        public void AddInterior();
        public void Spray();
        public void Tint();
    }
}
