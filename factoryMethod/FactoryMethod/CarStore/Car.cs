﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CarStore
{
    public abstract class Car : ICar
    {
        public IList<string> Options { get; set; }
        public Car(IList<string> options)
        {
            Options = options;
        }
        public abstract void AddInterior();
        public abstract void Spray();
        public abstract void Tint();
        public override string ToString()
        {
            return $"[{GetType().Name}] {string.Join<string>(", ", Options)}";
        }
    }
}
