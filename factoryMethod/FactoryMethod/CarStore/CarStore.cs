﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarStore
{
    public abstract class CarStore
    {
        public virtual ICar OrderCar(IList<string> options)
        {
            ICar car = CreateCar(options);

            car.AddInterior();
            car.Spray();
            car.Tint();

            Console.WriteLine("\nOrdering car with the following options:");
            foreach (string option in options)
            {
                Console.WriteLine($" - {option}");
            }

            return car;
        }

        public abstract ICar CreateCar(IList<string> options);
    }
}
