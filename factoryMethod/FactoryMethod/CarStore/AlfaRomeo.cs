﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CarStore
{
    public class AlfaRomeo : CarStore
    {
        public override ICar CreateCar(IList<string> options)
        {
            return new Brera(options);
        }
    }
}
