﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryMethod
{
    public class VeggieBurger : IBurger
    {
        public string Name { get => "Veggie burger"; }
        public string[] Ingredients { get => new string[] { "Bread", "Tomato", "Lettuce", "Ketchup", "Burger" }; }
    }
}
