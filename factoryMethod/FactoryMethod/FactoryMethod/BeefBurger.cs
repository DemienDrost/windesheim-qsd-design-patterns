﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryMethod
{
    public class BeefBurger : IBurger
    {
        public string Name { get => "Beef burger"; }
        public string[] Ingredients { get => new string[] { "Bread", "Cheese", "Tomato", "Lettuce", "Ketchup", "Beef", "Bacon" }; }
    }
}
