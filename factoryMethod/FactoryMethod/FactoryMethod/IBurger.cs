﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryMethod
{
    public interface IBurger
    {
        public string Name { get; }
        public string[] Ingredients { get; }
    }
}
