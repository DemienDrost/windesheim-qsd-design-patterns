﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryMethod
{
    public class BurgerFactory
    {
        public IBurger CreateBurger(string type)
        {
            if (type.Equals("Beef"))
            {
                return new BeefBurger();
            }
            if (type.Equals("Chicken"))
            {
                return new ChickenBurger();
            }
            if (type.Equals("Veggie"))
            {
                return new VeggieBurger();
            }

            throw new ArgumentException($"Burger type '{type}' is not supported");
        }
    }
}
