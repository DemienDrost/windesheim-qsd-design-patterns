﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryMethod
{
    public class ChickenBurger : IBurger
    {
        public string Name { get => "Chicken burger"; }
        public string[] Ingredients { get => new string[] { "Bread", "Tomato", "Lettuce", "Ketchup", "Chicken" }; }
    }
}
