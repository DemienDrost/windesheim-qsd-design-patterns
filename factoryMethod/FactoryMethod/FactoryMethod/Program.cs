﻿using System;

namespace FactoryMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to BurgerFactory!");
            Console.WriteLine("Which burger do you like to order?");
        
            string[] burgers = { "Beef", "Chicken", "Veggie" };
        
            foreach(var burger in burgers)
            {
                Console.WriteLine($"\t - {burger}");
            }
        
            while(true)
            {
                Console.Write("\nBurger choice: ");
                var selectedBurgerType = Console.ReadLine();
        
                var burgerFactory = new BurgerFactory();
                var selectedBurger = burgerFactory.CreateBurger(selectedBurgerType);
        
                Console.WriteLine($"You ordered: {selectedBurger.Name} with the following ingredients");
                foreach(string ingredient in selectedBurger.Ingredients)
                {
                    Console.WriteLine($"\t - {ingredient}");
                }
                Console.WriteLine("");
            }
        }

        //static void Main(string[] args)
        //{
        //    Console.WriteLine("Welcome to BurgerFactory!");
        //    Console.WriteLine("Which burger do you like to order?");
        //
        //    string[] burgers = { "Beef", "Chicken", "Veggie" };
        //
        //    foreach (var burger in burgers)
        //    {
        //        Console.WriteLine($"\t - {burger}");
        //    }
        //
        //
        //    while (true)
        //    {
        //        Console.Write("\nBurger choice: ");
        //        var selectedBurgerType = Console.ReadLine();
        //
        //        IBurger selectedBurger = null;
        //
        //        if (selectedBurgerType == "Beef")
        //        {
        //            selectedBurger = new BeefBurger();
        //        }
        //        if (selectedBurgerType == "Chicken")
        //        {
        //            selectedBurger = new ChickenBurger();
        //        }
        //        if (selectedBurgerType == "Veggie")
        //        {
        //            selectedBurger = new VeggieBurger();
        //        }
        //
        //        Console.WriteLine($"You ordered: {selectedBurger.Name} with the following ingredients");
        //        foreach (string ingredient in selectedBurger.Ingredients)
        //        {
        //            Console.WriteLine($"\t - {ingredient}");
        //        }
        //        Console.WriteLine("");
        //    }
        //}
    }
}
