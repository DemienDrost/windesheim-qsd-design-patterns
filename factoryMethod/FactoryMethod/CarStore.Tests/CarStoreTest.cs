using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace CarStore.Tests
{
    public class Tests
    {
        [Test]
        public void ShouldCreateX3Car()
        {
            CarStore factory = new Bmw();
            var car = factory.OrderCar(new List<string>() { "Wheels" });

            Assert.IsInstanceOf<X3>(car);
            Assert.AreEqual(new List<string>() { "Wheels", "Panorama roof" }, car.Options);
        }

        [Test]
        public void ShouldCreateBreraCar()
        {
            CarStore factory = new AlfaRomeo();
            var car = factory.OrderCar(new List<string>() { "Wheels" });

            Assert.IsInstanceOf<Brera>(car);
            Assert.AreEqual(new List<string>() { "Wheels" }, car.Options);
        }
    }
}