﻿using DesignPatterns;
using NUnit.Framework;

namespace DesignPatternsTest
{
	class BlenderTest
	{
		[SetUp]
		public void Setup()
		{
		}

		[Test]
		public void TestTurnOn()
		{
			Blender blender = new Blender();
			blender.IsBlenderOn = false;
			blender.TurnOn();
			Assert.IsTrue(blender.IsDeviceOn());
		}

		[Test]
		public void TestTurnOff()
		{
			Blender blender = new Blender();
			blender.IsBlenderOn = true;
			blender.TurnOff();
			Assert.IsFalse(blender.IsDeviceOn());
		}
	}
}
