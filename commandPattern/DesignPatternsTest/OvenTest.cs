﻿using DesignPatterns;
using NUnit.Framework;

namespace DesignPatternsTest
{
	class OvenTest
	{
		[SetUp]
		public void Setup()
		{			
		}

		[Test]
		public void TestTurnOn()
		{
			Oven oven = new Oven();
			oven.IsOvenOn = false;
			oven.TurnOn();
			Assert.IsTrue(oven.IsDeviceOn());
		}

		[Test]
		public void TestTurnOff()
		{
			Oven oven = new Oven();
			oven.IsOvenOn = true;
			oven.TurnOff();
			Assert.IsFalse(oven.IsDeviceOn());
		}
	}
}

