﻿using DesignPatterns;
using NUnit.Framework;
using System.Security.Cryptography.X509Certificates;

namespace DesignPatternsTest
{
	class BlenderOffCommandTest
	{
		[Test]
		public void TestExecute()
		{
			Blender blender = new Blender();
			blender.IsBlenderOn = true;
			BlenderOffCommand blenderOffCommand = new BlenderOffCommand(blender);
			blenderOffCommand.Execute();

			Assert.IsFalse(blender.IsDeviceOn());
		}
	}
}
