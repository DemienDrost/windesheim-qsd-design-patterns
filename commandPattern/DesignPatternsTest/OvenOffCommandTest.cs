﻿using DesignPatterns;
using NUnit.Framework;
using System.Security.Cryptography.X509Certificates;

namespace DesignPatternsTest
{
	class OvenOffCommandTest
	{
		[Test]
		public void TestExecute()
		{
			Oven oven = new Oven();
			oven.IsOvenOn = true;
			OvenOffCommand ovenOffCommand = new OvenOffCommand(oven);
			ovenOffCommand.Execute();

			Assert.IsFalse(oven.IsDeviceOn());
		}
	}
}
