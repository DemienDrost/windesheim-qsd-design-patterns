﻿using DesignPatterns;
using NUnit.Framework;
using System.Security.Cryptography.X509Certificates;

namespace DesignPatternsTest
{
	class BlenderOnCommandTest
	{
		[Test]
		public void TestExecute()
		{
			Blender blender = new Blender();
			BlenderOnCommand blenderOnCommand = new BlenderOnCommand(blender);
			blenderOnCommand.Execute();

			Assert.IsTrue(blender.IsDeviceOn());
		}
	}
}
