﻿using DesignPatterns;
using NUnit.Framework;
using System.Security.Cryptography.X509Certificates;

namespace DesignPatternsTest
{
	class OvenOnCommandTest
	{
		[Test]
		public void TestExecute()
		{
			Oven oven = new Oven();
			OvenOnCommand ovenOnCommand = new OvenOnCommand(oven);
			ovenOnCommand.Execute();

			Assert.IsTrue(oven.IsDeviceOn());
		}
	}
}
