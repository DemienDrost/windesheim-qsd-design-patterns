﻿using System;
using System.Security.Cryptography.X509Certificates;

namespace DesignPatterns
{
	public class Program
	{
		public static void KeyPress(Oven oven, Blender blender)
		{
			var key = Console.ReadKey().Key;

			switch (key)
			{
				case ConsoleKey.A:
					if (oven.IsDeviceOn() == false)
					{
						Command ovenOnCommando = new OvenOnCommand(oven);
						ovenOnCommando.Execute();
					}
					else
					{
						Command ovenOffCommando = new OvenOffCommand(oven);
						ovenOffCommando.Execute();
					}
					KeyPress(oven, blender);
					break;

				case ConsoleKey.B:
					if (blender.IsDeviceOn() == false)
					{
						Command blenderOnCommando = new BlenderOnCommand(blender);
						blenderOnCommando.Execute();
					}
					else
					{
						Command blenderOffCommando = new BlenderOffCommand(blender);
						blenderOffCommando.Execute();
					}
					KeyPress(oven, blender);
					break;

				default:
					break;
			}

		}
		static void Main(string[] args)
		{			
			Oven oven = new Oven();
			Blender blender = new Blender();
			
			Console.WriteLine("Kies een keukenmachine om aan te zetten: \n A: Oven \n B: Blender \n");
			KeyPress(oven,blender);
		}
	}
}
