﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns
{
	public class BlenderOnCommand : Command
	{
		Blender Blender;
		public void Execute()
		{
			Blender.TurnOn();
		}

		public BlenderOnCommand(Blender blender)
		{
			Blender = blender;
		}
	}
}
