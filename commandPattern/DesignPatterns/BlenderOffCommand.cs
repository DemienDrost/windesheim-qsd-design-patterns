﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns
{
	public class BlenderOffCommand : Command
	{
		Blender Blender;
		public void Execute()
		{
			Blender.TurnOff();
		}

		public BlenderOffCommand(Blender blender)
		{
			Blender = blender;
		}
	}
}
