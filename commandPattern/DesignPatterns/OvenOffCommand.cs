﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns
{
	public class OvenOffCommand : Command
	{

		Oven oven;
		
		public void Execute()
		{
			oven.TurnOff();
		}

		public OvenOffCommand(Oven oven) 
		{
			this.oven = oven;
		}
	}
}
