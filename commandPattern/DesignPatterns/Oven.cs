﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns
{
	public class Oven : KitchenDevice
	{
		public bool IsOvenOn { get; set; }

		public void TurnOn()
		{
			Console.WriteLine("\nJe hebt de oven aangezet.");
			IsOvenOn = true;
		}

		public void TurnOff()
		{
			Console.WriteLine("\nJe heb de oven uitgezet.");
			IsOvenOn = false;
		}

		public bool IsDeviceOn()
		{
			return IsOvenOn;
		}

		public Oven()
		{
			IsOvenOn = false;
		}
	}
}
