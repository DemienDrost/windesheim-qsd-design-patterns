﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns
{
	interface KitchenDevice
	{
		public void TurnOn();
		public void TurnOff();
		public bool IsDeviceOn();
	}
}
