﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns
{
	public class OvenOnCommand : Command
	{
		Oven Oven;
						
		public void Execute()
		{
			Oven.TurnOn();			
		}	
		
		public OvenOnCommand(Oven oven)
		{
			Oven = oven;
		}
	}
}
