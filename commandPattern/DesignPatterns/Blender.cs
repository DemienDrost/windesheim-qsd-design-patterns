﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DesignPatterns
{
	public class Blender : KitchenDevice
	{
		public bool IsBlenderOn { get; set; }
		public void TurnOn()
		{
			Console.WriteLine("\nJe hebt de blender aangezet.");
			IsBlenderOn = true;
		}

		public void TurnOff()
		{
			Console.WriteLine("\nJe hebt de blender uitgezet.");
			IsBlenderOn = false;
		}

		public bool IsDeviceOn()
		{
			return IsBlenderOn;
		}

		public Blender()
		{
			IsBlenderOn = false;
		}
	}
}
